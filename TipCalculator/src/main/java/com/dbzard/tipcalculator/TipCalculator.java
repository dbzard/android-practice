package com.dbzard.tipcalculator;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.TextChange;
import com.googlecode.androidannotations.annotations.ViewById;

@EActivity
public class TipCalculator extends Activity {
	// constants used when saving/restoring state
	private static final String BILL_TOTAL = "BILL_TOTAL";
	private static final String CUSTOM_PERCENT = "CUSTOM_PERCENT";

	private double currentBillTotal; // bill amount entered by the user
	private int currentCustomPercent; // tip % set with the SeekBar

	@ViewById
	EditText tip10EditText; // displays 10% tip

	@ViewById
	EditText total10EditText; // displays total with 10% tip

	@ViewById
	EditText tip15EditText; // displays 15% tip

	@ViewById
	EditText total15EditText; // displays total with 15% tip

	@ViewById
	EditText billEditText; // accepts user input for bill total

	@ViewById
	EditText tip20EditText; // displays 20% tip

	@ViewById
	EditText total20EditText; // displays total with 20% tip

	@ViewById
	TextView customTipTextView; // displays custom tip percentage

	@ViewById
	EditText tipCustomEditText; // displays custom tip amount

	@ViewById
	EditText totalCustomEditText; // displays total with custom tip

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		if (savedInstanceState == null) {
			currentBillTotal = 0.0; // initialize the bill amount to zero
			currentCustomPercent = 18; // initialize the custom tip to 18%
		} // end if
		else // app is being restored from memory, not executed from scratch
		{
			currentBillTotal = savedInstanceState.getDouble(BILL_TOTAL);
			currentCustomPercent = savedInstanceState.getInt(CUSTOM_PERCENT);
		} // end else

		SeekBar customSeekBar = (SeekBar) findViewById(R.id.customSeekBar);
		customSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				currentCustomPercent = seekBar.getProgress();
				updateCustom();
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
			}
		});
	}

	@TextChange(R.id.billEditText)
	void onBillTotalTextChange(CharSequence text) {
		try {
			currentBillTotal = Double.parseDouble(text.toString());
		} catch (Exception e) {
			currentBillTotal = 0.0;
		}
		updateStandard();
		updateCustom();
	}

	// updates 10, 15 and 20 percent tip EditTexts
	private void updateStandard() {
		double tenPercentTip = currentBillTotal * .1;
		double tenPercentTotal = currentBillTotal + tenPercentTip;
		tip10EditText.setText(String.format("%.02f", tenPercentTip));
		total10EditText.setText(String.format("%.02f", tenPercentTotal));

		double fifteenPercentTip = currentBillTotal * .15;
		double fifteenPercentTotal = currentBillTotal + fifteenPercentTip;
		tip15EditText.setText(String.format("%.02f", fifteenPercentTip));
		total15EditText.setText(String.format("%.02f", fifteenPercentTotal));

		double twentyPercentTip = currentBillTotal * .20;
		double twentyPercentTotal = currentBillTotal + twentyPercentTip;
		tip20EditText.setText(String.format("%.02f", twentyPercentTip));
		total20EditText.setText(String.format("%.02f", twentyPercentTotal));
	} // end method updateStandard

	// updates the custom tip and total EditTexts
	private void updateCustom() {
		customTipTextView.setText(currentCustomPercent + "%");
		double customTipAmount = currentBillTotal * currentCustomPercent * .01;
		double customTotalAmount = currentBillTotal + customTipAmount;
		tipCustomEditText.setText(String.format("%.02f", customTipAmount));
		totalCustomEditText.setText(String.format("%.02f", customTotalAmount));
	} // end method updateCustom

	// save values of billEditText and customSeekBar
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putDouble(BILL_TOTAL, currentBillTotal);
		outState.putInt(CUSTOM_PERCENT, currentCustomPercent);
	} // end method onSaveInstanceState
}
